/***********************************************************************************************************
 * File name: configFromMemory.h
 * Version: 1.0
 * Author: Nandan Ghawate
 * About the File: 
 * 1.Contains the function prototypes for Config modes for ESP8266 based smart devices
 * depending on preconfigured data on specific memory locations.
 * 2.Functionality contains configuring the module using the read (prewritten configurations) from 
 * memory to Hotspot/AP mode or Router Client /Wifi Client /Station mode or both of the modes mentioned.
 * 
 *********************************************************************************************************** 
 * 
 * DEPENDENCES:
 * #include <EEPROM.h> 
 * #include <ESP8266WiFi.h>
 * 
 * *********************************************************************************************************
 * The libraries have been tested on NodeMCU development board. However, the author 
 * disclaims any kind of hardware failure resulting out of usage of libraries, directly or indirectly 
 * Files may be subject to change without prior notice.
 * 
 * *********************************************************************************************************
 * 
 * GNU GENERAL PUBLIC LICENSE: 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. See <http://www.gnu.org/licenses/>.
 * Dependencies may or may not be covered under the same licences.
 * Errors and Suggestions should be reported to nandbot@live.com
 ************************************************************************************************************/
#ifndef nodemcuConfigFromMemory_h
#define nodemcuConfigFromMemory_h

#include <EEPROM.h> //arduino-nodemcu EEPROM dependancy file
#include <ESP8266WiFi.h> //nodemcu WiFi dependancy file

/************************************** Function Prototypes *************************************************/
void printReadData();
void readMemoryConfigs();
int testRouterConnection(void);
void configEspMode(int onOffFlag);
void writeDemoConfigsToMemory(String demoProductId,String demoVersionNo,String demoHotspotName,String demoHotspotPassword,String demoRouterName,String demoRouterPassword);
/************************************************************************************************************/

#endif

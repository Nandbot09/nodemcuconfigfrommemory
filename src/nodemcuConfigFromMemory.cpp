/***********************************************************************************************************
 * File name: configFromMemory.cpp
 * Version: 1.0
 * Author: Nandan Ghawate
 * About the File: 
 * 1.Contains library routines for Config modes for ESP8266 based smart devices
 * depending on preconfigured data on specific memory locations.
 * 2.Functionality contains configuring the module using the read (prewritten configurations) from 
 * memory to Hotspot/AP mode or Router Client /Wifi Client /Station mode or both of the modes mentioned.
 * 
 *********************************************************************************************************** 
 * 
 * The libraries have been tested on NodeMCU development board. However, the author 
 * disclaims any kind of hardware failure resulting out of usage of libraries, directly or indirectly 
 * Files may be subject to change without prior notice.
 * 
 * *********************************************************************************************************
 * 
 * GNU GENERAL PUBLIC LICENSE: 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. See <http://www.gnu.org/licenses/>.
 * Dependencies may or may not be covered under the same licences.
 * Errors and Suggestions should be reported to nandbot@live.com
 ************************************************************************************************************/
#include "nodemcuConfigFromMemory.h"

/*******************************************Important Declarations*******************************************/
String versionNo = ""; //Version Number
String productId= ""; //ProductId given by the company
String hotspotName = ""; //nodeMCU Hotspot Mode Name
String hotspotPassword = ""; //nodeMCU Hotspot Mode Password
String routerName = ""; //wifi router Name
String routerPassword = ""; //wifi router Password
int hotSpotStatusFlag=0; //hotspot status can be known
int routerConnectionStatusFlag=0; //hotspot status can be known
/******************************************Address Locations*************************************************/
int versionNoAddress = 0; //address for module number
int productIdAddress = 100; //address for productId
int hotspotNameAddress = 200; //address for nodeMCU Hotspot Mode Name
int hotspotPasswordAddress = 300; //address nodeMCU Hotspot Mode Password
int routerNameAddress = 400; //address for wifi router Name
int routerPasswordAddress = 500; //address wifi router Password
/***********************************************************************************************************/

/***********************************printReadData() Start of Function***************************************/
/* About:
 * print the read data from memory.
 * for debugging.*/
void printReadData() {
  Serial.begin(9600);
  delay(1000);
  Serial.println(" ");
  Serial.print("productId : ");
  Serial.println(productId ); //print productId from Memory
  Serial.print("versionNo : ");
  Serial.println(versionNo ); //print Module Number from Memory
  Serial.print("hotspotName: ");
  Serial.println(hotspotName ); //print nodeMCU Hotspot Mode Name
  Serial.print("hotspotPassword: ");
  Serial.println(hotspotPassword); //print nodeMCU Hotspot Mode Password
  Serial.print("routerName: ");
  Serial.println(routerName ); //print wifi router Name
  Serial.print("routerPassword: ");
  Serial.println(routerPassword); //print wifi router Password
}
/***********************************printReadData() End of Function*****************************************/



/********************************readMemoryConfigs() Start of Function**************************************/
/* About:
 * initiate EEPROM.
 * clear all strings.
 * read data from memory to respective strings.*/
void readMemoryConfigs(){
  EEPROM.begin(4096);
  delay(1000);
  /************Clear all Strings******************/
  versionNo = "";
  productId = "";
  hotspotName = "";
  hotspotPassword = "";
  routerName = "";
  routerPassword = "";
  /***********************************************/

  
  /***********READ CONFIGS FROM EEPROM************/
  for (int i = versionNoAddress; i < (versionNoAddress + 50); ++i) versionNo += char(EEPROM.read(i));//Read Version no from memory
  for (int i = productIdAddress; i < (productIdAddress + 50); ++i) productId += char(EEPROM.read(i));//Read Product ID from memory
  for (int i = hotspotNameAddress; i < (hotspotNameAddress + 32); ++i) hotspotName += char(EEPROM.read(i));//Read hotspotName from memory  
  for (int i = hotspotPasswordAddress; i < (hotspotPasswordAddress + 64); ++i) hotspotPassword += char(EEPROM.read(i));//Read hotspotPassword from memory
  for (int i = routerNameAddress; i < (routerNameAddress + 32); ++i) routerName += char(EEPROM.read(i));//Read hotspotName from memory
  for (int i = routerPasswordAddress; i < (routerPasswordAddress + 64); ++i) routerPassword += char(EEPROM.read(i));//Read hotspotPassword from memory
  /*************************************************/
 
  /*************clear string ends******************/ 
  versionNo = versionNo.c_str();
  productId = productId.c_str();
  hotspotName = hotspotName.c_str();
  hotspotPassword = hotspotPassword.c_str();
  routerName = routerName.c_str();
  routerPassword = routerPassword.c_str();
  /************************************************/
}
/***********************************readMemoryConfigs() End of Function************************************/


/*******************************testRouterConnection Start of function*************************************/
/* About:
* test if router is connected.
* if connected return 2 else 1 */
int testRouterConnection(void) {
  int connectFlag = 0;
  while ( connectFlag < 2 ) {   //Wait for Wifi to connect
    if (WiFi.status() == WL_CONNECTED) {
      //Serial.println(WiFi.localIP().toString());
      return (2);
    }
    delay(1000);
    connectFlag++;
    //Serial.println(".");
  }
  if (WiFi.status() != WL_CONNECTED) return (1);//Connection timeout
 // return 0;
}
/********************************testRouterConnection End of function*************************************/


/********************************configEspMode() Start of Function****************************************/
/* About:
 * create or stop hotspot
 * create or connect to router
 * choice between AP, STA or AP_STA mode
 * depending upon onOffFlag.*/
void configEspMode(int onOffFlag) {
  if(onOffFlag==1){
    WiFi.mode(WIFI_AP);//Turn on Hotspot by activating Access Point Mode
    delay(500);
    WiFi.softAP(hotspotName, hotspotPassword);//configure hotspot with configs read from Memory
    hotSpotStatusFlag = 1;
    routerConnectionStatusFlag = 0;
    delay(500);
    //Serial.println(WiFi.softAPIP().toString());
  }
  else if(onOffFlag==2){
    WiFi.mode(WIFI_STA);
    delay(500);
    //if ((WiFi.status() != WL_CONNECTED))
    {
      if ( routerName.length() > 1 ) {
        WiFi.begin(routerName.c_str(), routerPassword.c_str());
        if ( testRouterConnection() == 2 ) {
          hotSpotStatusFlag = 0;
          routerConnectionStatusFlag = 1;
          return;
        }
        else{
          //Serial.println("Router connection failed please check Id and Password");
        }
      }
    }
    delay(500);
  }
  else if(onOffFlag==3){
    WiFi.mode(WIFI_AP_STA);
    delay(500);
    /*******************hotspot***************/
    WiFi.softAP(hotspotName, hotspotPassword);//configure hotspot with configs read from Memory
    hotSpotStatusFlag = 1;
    delay(500);
    //Serial.println(WiFi.softAPIP().toString());
    /**********connect to router**************/
    if ((WiFi.status() != WL_CONNECTED))
    {
      if ( routerName.length() > 1 ) {
        WiFi.begin(routerName.c_str(), routerPassword.c_str());
        if ( testRouterConnection() == 2 ) {
          routerConnectionStatusFlag = 1;
          return;
        }
        else{
          //Serial.println("Router connection failed please check Id and Password");
        }
      }
    }
    delay(500);
  }   
}
/***********************************configEspMode() End of Function**************************************/


/****************************writeDemoConfigsToMemory() Start of Function********************************/
/* About:
 * write input democonfigs to test module.
 * read and print data.
 * debugging only.*/
void writeDemoConfigsToMemory(String demoProductId,String demoVersionNo,String demoHotspotName,String demoHotspotPassword,String demoRouterName,String demoRouterPassword){
  for (unsigned int i = 0; i < 4096; ++i) EEPROM.write(i, 0);//Erase EEPROM
  for (unsigned int i = 0; i < demoVersionNo.length(); ++i) EEPROM.write(i+versionNoAddress, demoVersionNo[i]);
  for (unsigned int i = 0; i < demoProductId.length(); ++i) EEPROM.write(i+productIdAddress, demoProductId[i]);
  for (unsigned int i = 0; i < demoHotspotName.length(); ++i)EEPROM.write(i+hotspotNameAddress, demoHotspotName[i]);
  for (unsigned int i = 0; i < demoHotspotPassword.length(); ++i)EEPROM.write(i+hotspotPasswordAddress, demoHotspotPassword[i]);
  for (unsigned int i = 0; i < demoRouterName.length(); ++i)EEPROM.write(i+routerNameAddress, demoRouterName[i]);
  for (unsigned int i = 0; i < demoRouterPassword.length(); ++i)EEPROM.write(i+routerPasswordAddress, demoRouterPassword[i]);
  EEPROM.commit();
  delay(1000);
  readMemoryConfigs();
  delay(1000);
  printReadData();
  delay(1000);
}
/********************************writeDemoConfigsToMemory() End of Function*******************************/
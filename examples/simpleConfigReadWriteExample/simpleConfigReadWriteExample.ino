/***********************************************************************************************************
 * File name: nodemcuConfigFromMemory.cpp // EXAMPLE FILE for use of configFromMemory.h 
 * Version: 1.0
 * Author: Nandan Ghawate
 * About the File: 
 * This is an example file on how to use 
 * 
 *********************************************************************************************************** 
 * 
 * The libraries have been tested on NodeMCU development board. However, the author 
 * disclaims any kind of hardware failure resulting out of usage of libraries, directly or indirectly 
 * Files may be subject to change without prior notice.
 * 
 * *********************************************************************************************************
 * 
 * GNU GENERAL PUBLIC LICENSE: 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU 
 * General Public License as published by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. See <http://www.gnu.org/licenses/>.
 * Dependencies may or may not be covered under the same licences.
 * Errors and Suggestions should be reported to nandbot@live.com 
 ************************************************************************************************************/
#include "nodemcuConfigFromMemory.h"
void setup() {
  readMemoryConfigs();//Read the pre-saved memory configs
  delay(1000);
  printReadData();//Print the data that has been read
  delay(1000);
  //writeDemoConfigsToMemory("V1.1","nodeMcu", "Hotspot", "HPassword","Router","RPassword"); //write demo configs for testing
  configEspMode(3);//config nodemcu as AP_STA mode
}

void loop() {
} 